<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <title>{{Lang::get('messages.title')}}</title>
    <style type="text/css">
        @font-face {
            font-family: 'ALongTimeAgo';
            src: url("{{ url('fonts/serifGothicStd/SerifGothicStd.otf')}}") format("opentype"); /* Open Type Font */
            src: url('{{ url('fonts/serifGothicStd/SerifGothicStd.eot')}}'); /* IE9 Compat Modes */
            src: url('{{ url('fonts/serifGothicStd/SerifGothicStd.eot')}}?#iefix') format('embedded-opentype'), /* IE6-IE8 */
            url('{{ url('fonts/serifGothicStd/SerifGothicStd.woff')}}') format('woff'), /* Modern Browsers */
            url('{{ url('fonts/serifGothicStd/SerifGothicStd.ttf')}}')  format('truetype'), /* Safari, Android, iOS */
            url('{{ url('fonts/serifGothicStd/SerifGothicStd.svg') }}#svgFontName') format('svg'); /* Legacy iOS */
        }
        @font-face {
            font-family: 'StarWarsTypo';
            src: url('{{ url('fonts/starJediHollow/Starjhol.ttf')}}')  format('truetype');
        }
        html, body {
            font-weight: 100;
            height: 100vh;
            background-color: #231F20;
            color: #636b6f;
            margin: 0;
        }
        .full-height {
            height: 100vh;
        }
        .full-width{
            width: 100vw;
        }
        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }
        .position-ref {
            position: relative;
        }
        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }
        .content {
            margin: 0 auto;
            max-width: 64vw;
        }
        .centered{
            margin: 0 auto;
        }
        .crawl-perspective{
            zoom: 108%;
            position: absolute;
            -webkit-transform: translate3d(0, 5vh, 0) perspective(300px) rotateX(25deg);
            -moz-transform: translate3d(0, 5vh, 0) perspective(300px) rotateX(25deg);
            -ms-transform: translate3d(0, 5vh, 0) perspective(300px) rotateX(25deg);
            -o-transform: translate3d(0, 5vh, 0) perspective(300px) rotateX(25deg);
            transform: translate3d(0, 5vh, 0) perspective(300px) rotateX(25deg);
            perspective-origin: 50% 70%;
            -webkit-perspective: 160px;
            -webkit-perspective-origin: 50% 70%;
            -moz-perspective: 160px;
            -moz-perspective-origin: 50% 70%;
            height: 1000px;
            transform-origin: 50% 70%;
        }
        .crawl {
            position: relative;
            -webkit-animation: scrollText 80s linear 10s ;
            -moz-animation: scrollText 80s linear 10s;
            -o-animation: scrollText 80s linear 10s;
            animation: scrollText 80s linear 10s;
            margin:0 10vw;
            color:#EFC524;
            text-align: center;
            font-size: 5em;
            top: 200%;
        }
        .crawl h1{
            text-transform: uppercase;
        }
        .crawl p{
            text-align: justify;
        }
    </style>
    <link rel="stylesheet" href="{{ URL::asset('css/starwars.css') }}" />
</head>
<body>
<div id="markup" class="flex-center position-ref full-height">
    <div class="content">
        <div class="customfont fade-in-effect">
            {{Lang::get('starwarsquotes.IntroString')}}
        </div>
    </div>
</div>
</body>
<script src="{{url('js/jquery-3.2.0.min.js')}}"></script>
<script src="{{url('js/html-divs.js')}}"></script>
<script>
    $(document).ready(function(){
        var text ="<p class='closer'>{{ html_entity_decode(Lang::get('starwarsquotes.MainTitle1'))}}</p>"+
                "<p>{{ Lang::get('starwarsquotes.MainTitle2')}}</p>" ;
        var description = "<div id='crawl-perspective' class='crawl-perspective'><div class='crawl'><h3>{{ Lang::get('starwarsquotes.Episode')}}</h3>"+
                "<h1>{{ Lang::get('starwarsquotes.Title')}}</h1>";
        var paragraphs=[];
        @foreach( Lang::get('starwarsquotes.Text') as $text)
            paragraphs.push("<p>{{$text}}</p");
        @endforeach
        for (var i in paragraphs) {
            description+=paragraphs[i];
        }
        paragraphs+="</div></div>";
        description+=paragraphs;
        var snd = new Audio("{{url('audio/IntroStarWars.mp3')}}");
        function fadeTitle(){
            $('#mainTitle').removeClass().addClass('StarWars titleFadeOut zoomOut').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).remove();
            });
            setTimeout(function() {
                console.log(description);
                $('body').prepend(description);
            }, 3000);
        }
        function showTitles(title){
            snd.play();
            var mainTitle=$('<div/>', {
                id: 'mainTitle',
                class: 'StarWars',
                html: title
            });
            $('.content').append(mainTitle);
            $(".content").first().attr('class','centered');
            fadeTitle();
        }
        $('.fade-in-effect').hide(0).delay(300).fadeIn(1300,function() {
            $('.fade-in-effect').delay(3300).fadeOut(900, function() { swapBackground(); $(this).remove(); showTitles(text);});
        });
    });
</script>