/**
 * Created by allezallez on 29/03/17.
 */

function swapBackground() {
    var stars=Math.floor(Math.random() * 700) + 200;
    $('body').css('background','black');
    var starcanvas = $('<canvas/>',{
        id: 'starCanvas',
        style: 'border: 1px solid white; position:absolute;'
    }).prop({height:$( window ).height()-5,
        width:$( window ).width()-5});
    $('body').prepend(starcanvas);
    var c=document.getElementById("starCanvas");
    for(var i=0;i<=stars;i++){
        var getx=Math.floor(Math.random()*$(window).width())+1,gety=Math.floor(Math.random()*$(window).height())+1;
        point(getx,gety,c.getContext("2d"));
    }
}
function point(x, y, canvas){
    canvas.strokeStyle= "#FFFFFF";
    canvas.strokeRect(x,y,Math.floor(Math.random() * 1.1)+0.1,Math.floor(Math.random() * 1.1)+0.1);
}
