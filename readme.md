## Animacion del Intro de Star Wars

- [Alejandro Lopez Perez](https://bitbucket.org/allez99).

Se elige un Laravel 5.4 para server en el puerto 5000 y dar acceso publico por tuneles de ngrok.
Desde el la ruta inicial comienza la animacion del opening de Star Wars.

Para iniciar solo se requiere de

-composer install
-php artisan serve --port 5000
-[Instalar y correr ./ngrok http 5000](https://ngrok.com/)
